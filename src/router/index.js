import { createRouter, createWebHistory } from 'vue-router'

import store from '../store'
import TestDashboard from '../components/test-dashboard'
import NotFound from '../components/not-found'
import UserDashboard from '../components/user-dashboard'
import {
  ROUTE_SIGN_IN,
  ROUTE_SIGN_UP,
  ROUTE_DASHBOARD,
} from './constants'

const routes = [
  {
    path: '/sign-up',
    component: () => import('../components/auth/sign-up'),
    name: 'SignUp',
  },
  {
    path: ROUTE_SIGN_IN,
    component: () => import('../components/auth/sign-in'),
    name: 'SignIn',
  },
  {
    path: '/',
    component: () => import('../components/auth/sign-in'),
    name: 'SignInBase',
  },
  {
    path: ROUTE_DASHBOARD,
    component: TestDashboard,
    name: 'TestDashboard',
  },
  {
    path: '/user-dashboard',
    component: UserDashboard,
    name: 'UserDashboard',
  },
  {
    path: '/404NotFound',
    component: NotFound,
    name: '404NotFound',
  },
  {
    path: '/:catchAll(.*)',
    component: NotFound,
    name: 'NotFound',
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes,
})

router.beforeEach((
  to,
  from,
  next,
  ) => {
  const isAuthenticated = store.getters['auth/isAuthenticated']
  const authPaths = [ROUTE_SIGN_IN, ROUTE_SIGN_UP]

  if (isAuthenticated) {
    if (authPaths.includes(to.path)) {
      next(ROUTE_DASHBOARD)
    } else {
      next()
    }
  } else if (to.path === ROUTE_SIGN_IN || to.path === ROUTE_SIGN_UP) {
    next()
  } else {
    next(ROUTE_SIGN_IN)
  }
})

export default router
