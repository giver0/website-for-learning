import { useStore } from 'vuex'

const MODULE_NAME = 'users'

const useUsers = () => {
  const store = useStore()

  const requestUsers = () => store.dispatch(`${MODULE_NAME}/requestUsers`)
  const users = store.getters[`${MODULE_NAME}/users`]

  return {
    requestUsers,
    users,
  }
}

export default useUsers
