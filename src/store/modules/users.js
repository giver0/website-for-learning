import axios from 'axios'

const actions = {
  async requestUsers({ commit }) {
    await axios('https://jsonplaceholder.typicode.com/users')
    .then(response => {
      commit('updateUsers', response.data)
    })
  },

}

const mutations = {
  updateUsers(state, users) {
    users.map(user => state.users.push(user))
  },
}

const state = {
  users: [],
  limit: 3,
}

const getters = {
  users(state) {
    return state.users
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
