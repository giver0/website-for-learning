import auth from './auth'
import posts from './posts'
import users from './users'
import projects from './projects'

const modules = {
  auth,
  posts,
  users,
  projects,
}

export default modules
