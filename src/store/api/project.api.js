import backendApiClient from './backend-api-client'

const ENDPOINTS = {
  PROJECTS: `/group/2/project`,
}

export const requestProjects = () => (
  backendApiClient.get(ENDPOINTS.PROJECTS)
)

export const postProject = () => (
  backendApiClient.post(ENDPOINTS.PROJECTS)
)

export const deleteProject = (id) => {
  const PROJECT_BY_ID = `${ENDPOINTS.PROJECTS}/${id}`
  return backendApiClient.delete(PROJECT_BY_ID)
}

export const archiveProject = (id, payload) => {
  const PROJECT_BY_ID = `${ENDPOINTS.PROJECTS}/${id}`
  return backendApiClient.patch(PROJECT_BY_ID, payload)
}
